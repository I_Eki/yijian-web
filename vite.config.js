module.exports = {
  open: true,
  proxy: {
    '/socket.io': {
      target: 'http://localhost:830',
      changeOrigin: true,
      ws: true,
      pathRewrite: {
        '^/socket.io': '/socket.io'
      },
    }
  }
}
