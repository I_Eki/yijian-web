import { io } from 'socket.io-client'

const socket = io();

socket.on('msg', res => {
  console.log(res)
})

export { socket }
